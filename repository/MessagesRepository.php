<?php

namespace app\repository;

use app\entities\Message;
use app\services\NotFoundExeption;

class MessagesRepository
{

    public function save(Message $message): void
    {
        if (!$message->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
    }

    public function delete(Message $message): void
    {
        if (!$message->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }

    public function findAll(array $condition = null): array
    {
        return Message::find()
                ->andWhere($condition)
                ->orderBy(['id' => SORT_DESC])
                ->all();
    }

    public function findMessageById($id): Message
    {
        if (!$message = Message::find()->andWhere(['id' => $id])->one()) {
            throw new NotFoundExeption('Сообщение не найдено');
        }
        return $message;
    }

}
