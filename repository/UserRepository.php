<?php

namespace app\repository;

use app\entities\User;
use app\services\NotFoundExeption;

class UserRepository
{

    public function findByUsername($username): User
    {
        if (!$user = User::find()->andWhere(['username' => $username])->one()) {
            throw new NotFoundExeption('Пользователь не найден');
        }
        return $user;
    }

    public function findById($id): User
    {
        if (!$user = User::find()->andWhere(['id' => $id])->one()) {
            throw new NotFoundExeption('Пользователь не найден');
        }
        return $user;
    }

    public function save(User $user): void
    {
        if (!$user->save()) {
            throw new \DomainException('Ошибка при сохранении пользователя');
        }
    }

    public function remove(User $user): void
    {
        if (!$user->delete()) {
            throw new \DomainException('Ошибка при удалении пользователя');
        }
    }

}
