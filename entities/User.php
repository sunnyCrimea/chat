<?php

namespace app\entities;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property int $created_at
 */
class User extends ActiveRecord implements IdentityInterface
{

    const ROLE_ADMIN = 'admin';
    const ROLE_DEFAULT = 'default';

    public static function signup(string $username, string $password): self
    {
        $user = new static();

        $user->username = $username;
        $user->setPassword($password);
        $user->auth_key = Yii::$app->security->generateRandomString();
        return $user;
    }

    public function isAdmin(): bool
    {
        $role = Yii::$app->authManager->getAssignment(self::ROLE_ADMIN, $this->getId());

        if (!$role) {
            return false;
        }
        return true;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    private function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
    
    public function attributeLabels(): array
    {
        return [
            'username' => 'Пользователь',
            'created_at' => 'Дата регистрации',
            
        ];
    }

}
