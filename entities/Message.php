<?php

namespace app\entities;

use yii\db\ActiveRecord;
use app\entities\User;
use yii\db\BaseActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "messages".
 *
 * @property int $id
 * @property int $user_id
 * @property string $content
 * @property int $status
 * @property int $created_at
 *
 * @property Users $user
 */
class Message extends ActiveRecord
{

    const STATUS_AVAIBLE = 1;
    const STATUS_DISABLE = 0;

    public static function create(string $content, User $user): self
    {
        $message = new static();
        $message->content = $content;
        $message->status = self::STATUS_AVAIBLE;
        $message->user_id = $user->getId();
        return $message;
    }

    public function complaint(): void
    {
        if (!$this->isAvaible()) {
            throw new \DomainException('Сообщение уже отмечено нежелательным');
        }
        $this->status = self::STATUS_DISABLE;
    }

    public function approve(): void
    {
        if (!$this->isDisable()) {
            throw new \DomainException('Сообщение уже одобрено');
        }
        $this->status = self::STATUS_AVAIBLE;
    }

    public function isAvaible(): bool
    {
        return $this->status === self::STATUS_AVAIBLE;
    }

    public function isDisable(): bool
    {
        return $this->status === self::STATUS_DISABLE;
    }

    public static function tableName()
    {
        return 'messages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
    
    public function attributeLabels(): array
    {
        return [
            'user_id' => 'Пользователь',
            'content' => 'Сообщение',
            'created_at' => 'Добавлено'
        ];
    }

    public function getUsers()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
