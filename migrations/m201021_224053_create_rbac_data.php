<?php

use yii\db\Migration;
use app\entities\User;

/**
 * Class m201021_224053_create_rbac_data
 */
class m201021_224053_create_rbac_data extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $viewUsers = $auth->createPermission('viewUsers');
        $auth->add($viewUsers);

        $viewBlackList = $auth->createPermission('viewBlackList');
        $auth->add($viewBlackList);

        $viewBlackMsg = $auth->createPermission('viewBlackMsg');
        $auth->add($viewBlackMsg);

        //
        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $auth->addChild($admin, $viewUsers);
        $auth->addChild($admin, $viewBlackList);
        $auth->addChild($admin, $viewBlackMsg);

        $user = new User([
            'username' => 'Admin',
            'password_hash' => '$2y$13$P9.d7KUb8C6BHCvkdzMsrOi5U.vIAw01UmriB.34PiN50e8nTGFge', // 111111
            'auth_key' => Yii::$app->security->generateRandomString(),
            'role' => 'admin'
        ]);
        $user->save();
        $auth->assign($admin, $user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201021_224053_create_rbac_data cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m201021_224053_create_rbac_data cannot be reverted.\n";

      return false;
      }
     */
}
