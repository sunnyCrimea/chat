<?php

namespace app\helpers;

class CutUsername
{
    public static function cutUsername(string $username): string
    {
        $letter = mb_substr($username,0,1);
        return mb_strtoupper($letter);
    }
}
