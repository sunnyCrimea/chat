<?php

namespace app\controllers;

use Yii;
use app\entities\Message;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\services\MessagesService;
use yii\filters\AccessControl;

class ManageController extends Controller
{

    private $messagesService;

    public function __construct($id, $module, MessagesService $messagesService, $config = array())
    {
        $this->messagesService = $messagesService;
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Message::find()
                    ->andWhere(['status' => Message::STATUS_DISABLE])
                    ->orderBy(['created_at' => SORT_DESC]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionApprove($id)
    {
        try {
            $this->messagesService->approve($id);
            Yii::$app->session->setFlash('success', 'Сообщение одобрено');
            return $this->redirect(['/manage/index']);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->refresh();
        }
    }

    public function actionDelete($id)
    {
        $this->messagesService->delete($id);

        return $this->redirect(['index']);
    }

}
