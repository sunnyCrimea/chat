<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\services\AuthService;
use app\forms\auth\LoginForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class AuthController extends Controller
{

    private $authService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['login'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, AuthService $service, $config = array())
    {
        $this->authService = $service;
        parent::__construct($id, $module, $config);
    }

    public function actionLogin()
    {
        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->authService->auth($form);
                Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
                return $this->goHome();
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        $form->password = '';
        return $this->render('index', [
                    'model' => $form,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
