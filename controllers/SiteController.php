<?php

namespace app\controllers;

use yii\web\Controller;
use app\services\MessagesService;
use app\forms\chat\SendForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class SiteController extends Controller
{

    private $messagesService;

    public function __construct($id, $module, MessagesService $messagesService, $config = array())
    {
        $this->messagesService = $messagesService;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $form = new SendForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            if (Yii::$app->user->isGuest) {
                throw new ForbiddenHttpException('Access denied');
            }
            try {
                $this->messagesService->send($form);
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('index', [
                    'model' => $form,
                    'messages' => (Yii::$app->user->can('admin')) ? $this->messagesService->getMsgForAdmin() : $this->messagesService->getAllMessages(),
        ]);
    }

    public function actionComplaint($id)
    {
        try {
            $message = $this->messagesService->getMessage($id);
            $this->messagesService->complaint($message);
            Yii::$app->session->setFlash('success', 'Сообщение помечено как нежелательное.');
            return $this->goHome();
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->goHome();
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['complaint'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

}
