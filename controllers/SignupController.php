<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\services\SignupService;
use yii\filters\AccessControl;
use app\forms\auth\SignupForm;

class SignupController extends Controller
{

    private $signupService;

    public function __construct($id, $module, SignupService $service, $config = array())
    {
        $this->signupService = $service;
        parent::__construct($id, $module, $config);
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->signupService->signup($form);
                Yii::$app->session->setFlash('success', 'Вы успешно зарегестрировались!');
                return $this->goHome();
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('index', [
                    'model' => $form,
        ]);
    }

}
