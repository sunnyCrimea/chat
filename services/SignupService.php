<?php

namespace app\services;

use app\entities\User;
use app\repository\UserRepository;
use app\forms\auth\SignupForm;

class SignupService
{

    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function signup(SignupForm $form): void
    {
        $user = User::signup(
                        $form->username,
                        $form->password
        );

        $this->users->save($user);
    }

}
