<?php

namespace app\services\manage;

use Yii;
use app\repository\UserRepository;
use app\forms\manage\UserForm;
use app\services\NotFoundExeption;

class UserManageService
{

    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function setRole($id, UserForm $form): void
    {
        $user = $this->users->findById($id);
        if ($form->role == $user::ROLE_DEFAULT) {
            Yii::$app->authManager->revokeAll($user->getId());
            return;
        }
        if (!$role = Yii::$app->authManager->getRole($form->role)) {
            throw new NotFoundExeption('Роль не найдена');
        }
        Yii::$app->authManager->assign($role, $user->getId());
    }
    
    public function delete($id): void
    {
        $user = $this->users->findById($id);
        $this->users->remove($user);
    }

}
