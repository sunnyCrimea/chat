<?php

namespace app\services;

use app\repository\UserRepository;
use app\forms\auth\LoginForm;
use app\entities\User;

class AuthService
{

    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function auth(LoginForm $form): User
    {
        $user = $this->users->findByUsername($form->username);
        if (!$user || !$user->validatePassword($form->password)) {
            throw new \DomainException('Неверный логин или пароль!');
        }
        return $user;
    }

}
