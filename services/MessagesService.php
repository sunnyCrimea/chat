<?php

namespace app\services;

use app\forms\chat\SendForm;
use app\repository\MessagesRepository;
use app\entities\Message;
use Yii;

class MessagesService
{

    private $messages;

    public function __construct(MessagesRepository $messages)
    {
        $this->messages = $messages;
    }

    public function send(SendForm $form): void
    {
        $messages = Message::create(
                        $form->content,
                        Yii::$app->user->identity
        );

        $this->messages->save($messages);
    }

    public function delete($id): void
    {
        $message = $this->messages->findMessageById($id);
        $this->messages->delete($message);
    }

    public function complaint(Message $message): void
    {
        $message->complaint();
        $this->messages->save($message);
    }

    public function approve($id): void
    {
        $message = $this->getMessage($id);
        $message->approve($message);
        $this->messages->save($message);
    }

    public function getAllMessages(): ?array
    {
        return $this->messages->findAll(['status' => Message::STATUS_AVAIBLE]) ?: null;
    }
    public function getMsgForAdmin(): ?array
    {
        return $this->messages->findAll() ?: null;
    }

    public function getMessage($id): Message
    {
        return $this->messages->findMessageById($id);
    }

}
