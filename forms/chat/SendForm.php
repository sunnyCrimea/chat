<?php

namespace app\forms\chat;

use yii\base\Model;

class SendForm extends Model
{

    public $content;

    function rules()
    {
        return [
            ['content', 'required'],
            [['content'], 'string', 'max' => 255],
        ];
    }

}
