<?php

namespace app\forms\auth;

use yii\base\Model;

class LoginForm extends Model
{

    public $username;
    public $password;
    public $rememberMe = true;

    public function rules()
    {
        return [
            [['username', 'password'], 'required', 'message' => 'Поле не может быть пустым!'],
            ['rememberMe', 'boolean'],
        ];
    }

}
