<?php

namespace app\forms\auth;

use yii\base\Model;
use app\entities\User;

class SignupForm extends Model
{

    public $username;
    public $password;

    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Введите имя пользователя!'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => 'Имя пользователя занято!'],
            ['username', 'string', 'min' => 2, 'tooShort' => 'Введите минимум 2 символа!', 'max' => 20],
            ['password', 'required', 'message' => 'Введите пароль!'],
            ['password', 'string', 'min' => 4, 'tooShort' => 'Введите минимум 4 символа!', 'max' => 20],
        ];
    }

}
