<?php

namespace app\forms\manage;

use yii\base\Model;

class UserForm extends Model
{

    public $role;

    public function rules()
    {
        return [
            ['role', 'required'],
        ];
    }

}
