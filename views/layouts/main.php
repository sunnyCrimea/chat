<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin();
            NavBar::end();
            ?>

            <div class="container">
                <div class="container">
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-8 col-lg-offset-2">
                            <?= Alert::widget() ?>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-comment"></span> Чат
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-chevron-down"></span>
                                        </button>
                                        <ul class="dropdown-menu slidedown">
                                            <li><a href="<?= Url::to(['/site/index']); ?>"><span class="glyphicon glyphicon-refresh">
                                                    </span>Главная</a></li>
                                            <?php if (Yii::$app->user->can('admin')): ?>
                                                <li><a href="<?= Url::to(['/user/index']); ?>"><span class="glyphicon glyphicon-user">
                                                        </span>Пользователи</a></li>

                                                <li><a href="<?= Url::to(['/manage/index']); ?>"><span class="glyphicon glyphicon-remove">
                                                        </span>Черный список</a></li>
                                            <?php endif; ?>
                                            <li class="divider"></li>
                                            <?php if (Yii::$app->user->isGuest): ?>
                                                <li><a href="<?= Url::to(['/auth/login']); ?>"><span class="glyphicon glyphicon-off"></span>
                                                        Вход</a></li>
                                                <li><a href="<?= Url::to(['/signup/index']); ?>"><span class="glyphicon glyphicon-ok-sign">
                                                        </span>Регистрация</a></li>
                                            <?php else : ?>
                                                <li><a href="<?= Url::to(['/auth/logout']); ?>" data-method='POST'><span class="glyphicon glyphicon-off"></span>
                                                        Выход</a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->registerCssFile("@web/css/chat.css"); ?>
<?php $this->endPage() ?>
