<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Black list';
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'content' => function($data) {
                    return Html::a($data->users->username, ['/user/view', 'id' => $data->users->id]);
                }
            ],
            'content',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '&nbsp{approve}&nbsp&nbsp{delete}',
                'buttons' => [
                    'approve' => function($url, $data) {
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', ['/manage/approve', 'id' => $data->id]);
                    }
                ],
            ],
        ],
    ]);
    ?>


</div>
