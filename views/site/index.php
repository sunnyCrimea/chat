<?php
/* @var $this yii\web\View */
/* @var $model app\forms\chat\SendForm */

use yii\helpers\Html;
use app\helpers\CutUsername;

$this->title = 'Chat';
?>

<div class="panel-body body-panel">
    <ul class="chat">
        <?php if (!$messages) : ?>
            <p>Еще никто ничего не написал..</p>
        <?php else: ?>
            <?php foreach ($messages as $message): ?>
                <li class="left clearfix"><span class="chat-img pull-left">
                        <img src="http://placehold.it/50/55C1E7/fff&text=<?= CutUsername::cutUsername($message->users->username); ?>" alt="User Avatar" class="img-circle" />
                    </span>

                    <div class = "chat-body clearfix">
                        <div class = "header">
                            <strong class = "primary-font">
                                <?php if ($message->users->isAdmin()): ?>
                                <i><?= Html::encode($message->users->username); ?></i></strong> <i>(admin)</i>
                            <?php else: ?>
                                    <?= Html::encode($message->users->username); ?></strong>
                                <?php endif; ?>
                            <small class = "pull-right text-muted">
                                <?php if (Yii::$app->user->can($message->users::ROLE_ADMIN) && $message->isAvaible()): ?>        
                                    <?= Html::beginForm(['/site/complaint', 'id' => $message->id]); ?>    
                                    <span class = "glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asRelativeTime($message->created_at); ?>
                                    <?= Html::submitButton('Жалоба', ['class' => 'btn btn-danger btn-xs']); ?>
                                    <?= Html::endForm(); ?>
                                <?php else: ?>
                                    <span class = "glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asRelativeTime($message->created_at); ?>
                                <?php endif; ?>
                            </small>

                        </div>
                        <p>
                            <?php if ($message->users->isAdmin() && $message->isDisable()): ?>
                                <i><s><?= Html::a(Html::encode($message->content), ['/manage/index']); ?></s></i>
                            <?php elseif ($message->users->isAdmin()) : ?>
                                <i><?= Html::encode($message->content); ?></i>
                            <?php elseif ($message->isDisable()): ?>
                                <s><?= Html::a(Html::encode($message->content), ['/manage/index']); ?></s>
                            <?php else: ?>
                                <?= Html::encode($message->content); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
</div>
<div class = "panel-footer clearfix">
    <?php
    if (!Yii::$app->user->isGuest) {
        echo $this->render('_form', [
            'model' => $model,
        ]);
    }
    ?>

