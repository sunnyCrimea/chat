<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>


<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'content')->label('Сообщение')->textarea(['class' => 'form-control', 'rows' => '3']); ?>
<!--<textarea class = "form-control" rows = "3"></textarea>-->
<span class = "col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12" style = "margin-top: 10px">
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-warning btn-lg btn-block', 'id' => 'btn-chat']); ?>
    <?php $form = ActiveForm::end(); ?>
</span>

