<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\entities\User;

/* @var $this yii\web\View */
/* @var $model app\entities\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'role')->dropDownList([User::ROLE_ADMIN => 'Добавить в администраторы', User::ROLE_DEFAULT => 'Убрать из администраторов']) ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
